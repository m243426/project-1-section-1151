import logging
import parsing.CMD_parse as cmdParse
import parsing.RSP_parse as rspParse
import wep_implementation as Weapon

class WepsOps:
    def __init__(self, layer4, checkTSID, sendTSID):

        # Save the layer 4 object as a instance variable
        # so we can reference it later
        self.layer4 = layer4

        self.checkTSID = checkTSID
        self.sendTSID = sendTSID

        self.weapon = Weapon.Weapon()
        # Open a new socket to listen on. Here we're choosing
        # port 80 (just as an example.)
        self.layer4.connect_to_socket(80, self.receive)
    
        

    def receive(self, data):
        """Receive and handle a message.
        """

        fields = data.split('\n')
        sender = fields[1].split()[1] 
        TSID = fields[2].split()[1]
        time = fields[3].split()[1]
        type = fields[4].split()[1]

        if TSID != self.checkTSID:
            failRSP = rspParse.buildRSP("weapon1", self.sendTSID, "FAIL", "AUTHENTICATION FAILURE: SYSTEM DOES NOT RECOGNIZE PROVIDED TSID")
            self.send(None, failRSP)   
        else:
            if type == "STANDBY":
                if self.weapon.getLaunchState() != "PASSIVE":
                    failRSP = rspParse.buildRSP("weapon1", self.sendTSID, "FAIL", "Cannot elevate to STANDBY when launchstate is not PASSIVE")
                    self.send(None, failRSP)
                else:
                    self.weapon.upgradeLaunchState()
                    successRSP = rspParse.buildRSP("weapon1", self.sendTSID, "SUCCESS", "STATREP : Code 20 : System initiated & awaiting target // Not ready to fire.")
                    self.send(None, successRSP)

            elif type == "TARGET":
                if self.weapon.getLaunchState() != "STANDBY":
                    failRSP = rspParse.buildRSP("weapon1", self.sendTSID, "FAIL", "Cannot elevate to TARGETED when launchstate is not STANDBY")
                    self.send(None, failRSP)
                else:
                    self.weapon.upgradeLaunchState()
                    lat = fields[5].split()[1]
                    lon = fields[6].split()[1]
                    self.weapon.setTargets(lat, lon)
                    successRSP = rspParse.buildRSP("weapon1", self.sendTSID, "SUCCESS", "STATREP : Code 100 : All systems capable // READY TO FIRE.")
                    self.send(None, successRSP)
            elif type == "FIRE":
                if self.weapon.getLaunchState() != "TARGETED":
                    failRSP = rspParse.buildRSP("weapon1", self.sendTSID, "FAIL", "Cannot FIRE when launchstate is not TARGETED")
                    self.send(None, failRSP)
                else:
                    launchCode = fields[5].split()[1]
                    print("launch code: " + launchCode)
                    if self.weapon.fireWeapon(launchCode) == True:
                        self.weapon.upgradeLaunchState()
                        successRSP = rspParse.buildRSP("weapon1", self.sendTSID, "SUCCESS", "STATREP : Code 999: Weapon launched successfully.")
                        self.send(None, successRSP)
                    else:
                        failRSP = rspParse.buildRSP("weapon1", self.sendTSID, "FAIL", "Incorrect launch code. FAILURE to launch")
                        self.send(None, failRSP)
    
    def send(self, receiver_addr, data):
        """Sends a message to a receiver.
        """

        # Send the message to layer 4. We haven't implemented
        # proper addresses yet so we're setting the port numbers
        # and addr to None.
        self.layer4.from_layer_5(data=data, src_port=None,
                dest_port=None, dest_addr=None)



# This program runs all the applications that will
# be running on our network stack.
from threading import Thread

import sys
sys.path.append('../')
import layer4.stub_layer_4 as Layer4
import layer5.parsing.CMD_parse as cmdParse
import layer5.parsing.RSP_parse as rspParse
import weps_ops_interaction as WepsOps

layer4 = Layer4.StubLayer4()

def initialSetup():

    wepsTSID = input("Weapon TSID: ") 
    opsTSID = input("OPS TSID: ")
    myAddress = "1.1.1.1"
    opsAddress = "2.2.2.2"

    return wepsTSID, opsTSID, myAddress, opsAddress

def start_server_client(checkTSID, sendTSID):
    server = WepsOps.WepsOps(layer4, checkTSID, sendTSID)
    print("OPS-connected server online.")
    while True:
        pass

wepsTSID, opsTSID, myAddress, opsAddress = initialSetup()
t = Thread(target=start_server_client, args = (opsTSID, wepsTSID,))
t.start()

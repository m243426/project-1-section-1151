from datetime import datetime

#turns a long string into chunks parsed by newlines, returns them as an array
def createChunks(txt):
    long_msg = txt
    msg_type = txt.split('\n')[0]
    nsender = txt.split('\n')[1]
    TSID = txt.split("\n")[2]
    timesent = txt.split("\n")[3]
    type = txt.split("\n")[4]
    timeOfCMD = txt.split("\n")[5]
    msg = txt.split("\n")[6]

    return msg_type, nsender, TSID, timesent, type, timeOfCMD, msg

#turns a list of chunks into a sendable message
def createMSG(list):
    long = ""
    for val in list:
        long += (val + "\n")
    long = long.strip()

    return long

def buildRSP(sender, TSID, type, error):
   return "RSP\n" + "sender: " + sender + "\nTSID: " + TSID + "\ntimesent: " + str(datetime.now()) + "\ntype: " + type + "\nmessage: " + error 

def parseRSP(rawRSP):
  fields = rawRSP.split('\n')
  sender = fields[1].split()[1] 
  TSID = fields[2].split()[1]
  timesent = fields[3].split()[1]
  type = fields[4].split()[1]
  msg_contents = fields[5]

  return sender, TSID, timesent, type, msg_contents


'''
text = "RSP\nsender:weapon1\nTSID:g9tmincJ?JdL8RHdJi56\ntimesent:08-OCT-2022 20:00:3000000\ntype:FAIL\ntimeofCMD:08-10-2022 20:00:15\nerrorMSG:CMD improperly formatted. Missing field lat"
chunks = createChunks(text)
print(chunks)

msg = createMSG(chunks)
print(msg)
'''

#STATREP → Code 100 : All systems capable // READY TO FIRE.
#STATREP → Code 20 : System initiated & awaiting target // Not ready to fire.
#error_codes = [[800, "Not ready to fire"], [200, "Ready to fire"], [777, "Launch impossible"], [999, "Launched successfully"], [100, "System idle"]]

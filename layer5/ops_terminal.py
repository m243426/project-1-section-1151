# This program runs all the applications that will
# be running on our network stack.
from threading import Thread

import sys
sys.path.append('../')
import layer4.stub_layer_4 as Layer4
import layer5.parsing.CMD_parse as cmdParse
import layer5.parsing.MSG_parse as msgParse
import layer5.parsing.RSP_parse as rspParse
import ops_weps_interaction as OpsWeps
import plainChat as PlainChat

layer4 = Layer4.StubLayer4()

def initialSetup():
    print("\u2727 OPS FLOOR TERMINAL \u2727")

    #TO DO: input checking 
    opsTSID = input("Your TSID: ") 
    potusTSID = input("POTUS's TSID: ")
    wepsTSID = input("WEPS's TSID:")
    myAddress = input("Address of this system: ")
    toPOTUSAddress = input("Address of POTUS: ")
    toWEPSAddress = input("Address of WEPS: ")

    return opsTSID, potusTSID, wepsTSID, myAddress, toPOTUSAddress, toWEPSAddress

def start_potus_server(potusTSID):
    server = PlainChat.PlainChat(layer4, potusTSID)
    print("POTUS-connected server online.")
    while True:
        pass

def start_weps_server(checkTSID, sendTSID):
    server = OpsWeps.OpsWeps(layer4, checkTSID, sendTSID)
    print("WEPS-connected server online.")
    while True:
        pass

def start_client(wepsTSID, potusTSID, sendTSID):

    wepsClient = OpsWeps.OpsWeps(layer4, wepsTSID, sendTSID)

    #REMOVE HERE
    #potusClient = PlainChat.PlainChat(layer4, potusTSID)

    while True:
        msg = input()

        #determine if command going to POTUS or WEPS with * syntax
        #Parse appropriately
        #send via wepsClient or potusClient

        if msg[0] == '*':
            msgType = msg.split()[0]
            if msgType == "*STANDBY":
                parsed_msg = cmdParse.standby("OPS", sendTSID, msg)
            elif msgType == "*TARGET":
                parsed_msg = cmdParse.target("OPS", sendTSID, msg)
            elif msgType == "*FIRE":
                parsed_msg = cmdParse.fire("OPS", sendTSID, msg)
            else:
                print("Error // unknown CMD type")
            wepsClient.send(None, parsed_msg)
        else:
            parsed_msg = msgParse.creatMsg("OPS", msg, sendTSID)

            #REMOVE HERE 
            #potusClient.send(None, parsed_msg)

opsTSID, potusTSID, wepsTSID, myAddress, toPOTUSAddress, toWEPSAddress = initialSetup()

#REMOVE HERE
#t = Thread(target=start_potus_server, args = (potusTSID,))
#t.start()


t = Thread(target=start_weps_server, args = (wepsTSID, opsTSID))
t.start()
t = Thread(target=start_client, args = (wepsTSID, potusTSID, opsTSID,))
t.start()

import logging

import parsing.CMD_parse as cmdParse
import parsing.RSP_parse as rspParse
class OpsWeps:
    def __init__(self, layer4, checkTSID, sendTSID):
        self.layer4 = layer4

        self.checkTSID = checkTSID
        self.sendTSID = sendTSID

        # Open a new socket to listen on. Here we're choosing
        # port 80 (just as an example.)
        self.layer4.connect_to_socket(80, self.receive)


    def receive(self, data):
        """Receive and handle a message.
        """
        sender, TSID, timesent, type, msg_contents = rspParse.parseRSP(data)
        if TSID == self.checkTSID:
            verified = "TSID VERIFIED"
        else:
            verified = "UNVERIFIED"
        if type == "SUCCESS":
            status = "SUCCESS"
        elif type == "FAIL":
            status = "FAILURE"
        else:
            status = "UNKNOWN"
        
        msg = msg_contents.split()[1:]
        readableMSG = ""
        for m in msg:
            readableMSG += (" " + m) 
        
        print(f"{verified} message from {sender} sent @ {timesent} with status {status}: {readableMSG}")


    def send(self, receiver_addr, data):

        # Send the message to layer 4. We haven't implemented
        # proper addresses yet so we're setting the port numbers
        # and addr to None.
        self.layer4.from_layer_5(data=data, src_port=None,
                dest_port=None, dest_addr=None)



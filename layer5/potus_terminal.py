from threading import Thread

import sys
sys.path.append('../')
import layer4.stub_layer_4 as Layer4
import layer5.parsing.MSG_parse as msgParse
import plainChat as PlainChat

layer4 = Layer4.StubLayer4()

def initialSetup():
    print("\u272F PRESIDENTIAL CHATROOM \u272F")

    #TO DO: input checking 
    potusTSID = input("Your TSID: ") 
    opsTSID = input("OPS's TSID: ")
    myAddress = input("Address of this system: ")
    toAddress = input("Address to send to: ")

    return potusTSID, opsTSID, myAddress, toAddress

def start_server(TSID):
    server = PlainChat.PlainChat(layer4, TSID)
    print("OPS-connected server online.")
    while True:
        pass

def start_client(checkTSID, sendTSID):
    # Start client message loop.
    client = PlainChat.PlainChat(layer4, checkTSID)
    while True:
        msg = input()
        # We haven't implemented an address scheme, so we will just
        # pass the receiver_addr = None.
        parsed_msg = msgParse.creatMsg("POTUS", msg, sendTSID)
        client.send(None, parsed_msg)


potusTSID, opsTSID, myAddress, toAddress = initialSetup()
t = Thread(target=start_server, args = (opsTSID,))
t.start()
t = Thread(target=start_client, args = (opsTSID,potusTSID))
t.start()

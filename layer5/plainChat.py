import logging
import layer5.parsing.MSG_parse as msgParse

class PlainChat:
    def __init__(self, layer4, TSID):
        """Create a client "process".

        We pass a layer 5 object so that we don't instantiate
        more than one "network stack."
        """

        self.TSID_key = TSID

        # Save the layer 4 object as a instance variable
        # so we can reference it later
        self.layer4 = layer4

        # Open a new socket to listen on. Here we're choosing
        # port 80 (just as an example.)
        self.layer4.connect_to_socket(80, self.receive)

    def parseMSG(data):
        return "message", 123456, "p"


    def receive(self, data):
        """Receive and handle a message.
        """
        send_user, TSID, time_sent, msg_contents = msgParse.returnInfo(data)

        verified = "TSID VERIFICATION FAILURE"
        if send_user == "POTUS":
            if TSID == self.TSID_key:
                verified = "TSID verified"
        elif send_user == "OPS":
            if TSID == self.TSID_key:
                verified = "TSID verified"
        else:
            send_user = send_user + " [INVALID USER]"
        print(f"\n<{send_user}> {verified} sent @ {time_sent}: {msg_contents}")

    def send(self, receiver_addr, data):
        """
        Sends a message to a receiver.
        """

        # Send the message to layer 4. We haven't implemented
        # proper addresses yet so we're setting the port numbers
        # and addr to None.
        self.layer4.from_layer_5(data=data, src_port=None,
                dest_port=None, dest_addr=None)



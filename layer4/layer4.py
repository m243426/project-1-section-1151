# This is a stub for Layer 4.
import sys, logging, hashlib, time, base64
from threading import Thread
sys.path.append('../')
import layer3.stub_layer_3 as Layer3

class Socket:
    def __init__(self):
        self.hash = b"\x00" * 16
        self.prior = b""
        self.data = b""
        self.layer5 = None
        self.dest_address = None
        self.dest_port = None
        
    def connect1(self, callback):
        self.layer5 = callback

    def connect2(self, dest_port, dest_address):
        self.status = 2
        self.dest_port = dest_port
        self.dest_address = dest_address
        self.header = self.dest_port.to_bytes(4,'little')+ self.port.to_bytes(4, 'little') + self.ip.to_bytes(4, 'little')

    def connect3(self, ip, port, callback):
        self.ip = ip
        self.status = 0
        self.port = port
        self.layer5 = callback
        self.timer = -1

    def connect4(self, ip, port, dest_address, dest_port):
        self.ip = ip
        self.status = 1
        self.port = port
        self.dest_address = dest_address
        self.dest_port = dest_port
        self.timer = 0
        self.header = self.dest_port.to_bytes(4,'little')+ self.port.to_bytes(4, 'little') + self.ip.to_bytes(4, 'little')

    def prior_packet(self):
        return base64.b64encode(self.hash + self.header + self.prior).decode('ascii')
    
    def next_packet(self):
        if(len(self.data) > 512):
            self.prior = self.data[:512]
            self.data = self.data[512:]
        elif(self.data == b""):
            self.timer = -1
            return self.nack()
        else:
            self.prior = self.data
            self.data = b""
        self.hash = hashlib.md5(self.hash + self.prior).digest()
        if(self.hash[:-1] == b"\x00" * 15):
            self.hash[-2] = b"\x01"
        return self.prior_packet()
    
    def special_packet(self, hsh):
        return base64.b64encode(hsh + self.header).decode('ascii')

    def receive_packet(self, hsh, data):
        if(hsh == self.hash and data == b""):
            return True
        temphsh = hashlib.md5(self.hash + data).digest()
        if(temphsh[:15] == b"\x00" * 15):
            temphsh[-2] = b"\x01"
        if(temphsh == hsh):
            if(self.layer5 != None):
                self.layer5(data.decode('ascii'))
            self.hash = hsh
            return True
        return False

    def nack(self):
        return self.special_packet(self.hash)

        

class Layer4:
    def __init__(self):
        """Create a Layer 4 Object.

            This initializer doesn't ask for a callback since there
            will likely be multiple callbacks connected to sockets.
            Use the `connect_to_socket` method to register a
            layer_5_cb.

        """
        # Connect to Layer 3
        self.sockets = []
        self.messages = []
        self.layer3 = Layer3.StubLayer3(self.from_layer_3)
        self.ip = self.layer3.send_ip()
        t = Thread(target = self.run, args = ())
        t.start()

    def connect_to_socket(self, port_number, layer_5_cb):
        """Connect an application to a socket.

        `port_number` - Integer.

        `layer_5_cb` - Function. This function will be called when data comes in on the specified port.
        """
        for i in self.sockets:
            if(i.port == port_number):
                if(i.layer5 == None):
                    i.connect1(layer_5_cb)
                else:
                    logging.debug(f"Cannot connect to port {port_number}, another socket is already connected to this port.")
                return
        newSock = Socket()
        newSock.connect3(self.ip, port_number, layer_5_cb)
        self.sockets.append(newSock)
    
    def from_layer_5(self, data, src_port, dest_port, dest_addr):
        """Call this function to send data to this layer from layer 5

        `src_port` - Integer. This is the port number on this machine
        `dest_port` - Integer. This is the port number on the
                      receiver's machine.
        """
        logging.debug(f"Layer 4 received msg from Layer 5: {data}")
        data = data.encode('ascii')
        msg = ""
        for i in self.sockets:
            if(i.port == src_port):
                if(i.dest_port == dest_port and i.dest_address == dest_addr):
                    i.data += data
                    if(i.timer < 0):
                        msg = i.next_packet()
                        i.timer = 0
                elif(i.dest_address == None):
                    i.data += data
                    i.connect2(dest_port, dest_addr)
                    msg = i.special_packet(b"\x00" * 16)
                    i.status = 1
                else:
                    logging.debug(f"Source port {src_port} has an established connection with a different host")
                    return
        if(msg == ""):
            sock = Socket()
            sock.connect4(self.ip, src_port, dest_addr, dest_port)
            sock.data += data
            self.sockets.append(sock)
            msg = sock.next_packet()
        self.layer3.from_layer_4(msg, dest_addr)

    def from_layer_3(self, data):
        """Call this function to send data to this layer from layer
        3
        """
        logging.debug(f"Layer 4 received msg from Layer 3: {data}")
        data = bytes(data, 'ascii')
        data = base64.b64decode(data)
        if(len(data) < 28):
            logging.debug("Improper packet received.")
            return
        hsh = data[:16]
        rcvprt = int.from_bytes(data[16:20], byteorder='little')
        sndprt = int.from_bytes(data[20:24], byteorder='little')
        sndadr = int.from_bytes(data[24:28], byteorder='little')
        info = data[28:]
        sock = None
        for i in self.sockets:
            if(i.port == rcvprt):
                sock = i
        self.messages.append({
            "hash": hsh,
            "receive_prt": rcvprt,
            "send_prt": sndprt,
            "send_addr": sndadr,
            "data": info,
            "socket": sock})

    def close(port):
        logging.debug("Closing connections has not yet been implemented.")

    def run(self):
        while(True):
            for i in self.messages:
                if(i["socket"] == None):
                    if(i["hash"] == b"\x00" * 16):
                        resp = b"\x00" * 15 + b"\x01" + sndprt.to_bytes(4, 'little') + rcvprt.to_bytes(4, 'little') + self.layer3.addr.to_bytes(4, 'little')
                        self.layer3.from_layer_4(base64.b64encode(resp).decode('ascii'), i["send_addr"])
                elif(i["hash"] == b"\x00" * 15 + b"\x01" and i["socket"].dest_address == i["send_addr"]):
                    sockets.remove(i["socket"])
                    logging.debug("Cannot connect, no open socket on receiver")
                elif(i["hash"] == b"\x00" * 15 + b"\x02" and i["socket"].dest_address == i["send_addr"]):
                    sockets.remove(i["socket"])
                    logging.debug("Socket on other end is occupied.")
                elif(i["socket"].status == 0 and i["hash"] == b"\x00" * 16):
                    i["socket"].connect2(i["send_prt"], i["send_addr"])
                    self.layer3.from_layer_4(i["socket"].special_packet(b"\x00" * 16), i["send_addr"])
                elif(i["socket"].status == 1 and i["hash"] == b"\x00" * 16 and i["send_addr"] == i["socket"].dest_address):
                    i["socket"].status = 2
                    if(i["socket"].data != b""):
                        i["socket"].timer = 0
                        self.layer3.from_layer_4(i["socket"].next_packet(), i["send_addr"])
                    else:
                        i["socket"].timer = -1
                elif(i["socket"].status == 2):
                    if(i["socket"].dest_address == i["send_addr"]):
                        if(i["socket"].receive_packet(i["hash"], i["data"])):
                            if(len(i["data"]) > 0 or i["socket"].data != b""):
                                self.layer3.from_layer_4(i["socket"].next_packet(), i["send_addr"])
                            else:
                                i["socket"].timer = -1
                        else:
                            self.layer3.from_layer_4(i["socket"].nack(), i["send_addr"])
                    else:
                        self.layer3.from_layer_4(i["socket"].special_packet(b"\x00" * 15 + b"\x02"), i["send_addr"])
            self.messages = []
            for i in self.sockets:
                if(i.timer >= 0):
                    i.timer += 1
                    if(i.timer > 100):
                        if(i.hash == b"\x00" * 16):
                            self.layer3.from_layer_4(i.special_packet(b"\x00" * 16), i.dest_address)
                        else:
                            self.layer3.from_layer_4(i.prior_packet(), i.dest_address)
                        i.timer = 0
            time.sleep(0.001)

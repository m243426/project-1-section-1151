#This is a stub for Layer 3.

# Import Layers
import sys
sys.path.append('../')
import layer2.stub_layer_2 as Layer2
import logging

# What our layer is responsible for:
# 1. adding header information that is easily parseable to relay information about the destination host and
# maybe the message that is being passed and send it to layer 4.
# 2. Parseing information from layer 2 that will tell us about the destination host and other aux informaiton.
# 3. Properly passing information on the correct interface to ensure that all nodes recieve the message. Sir 
# suggested that we flood the network and have some way to tell if we have already seen a message so we dont 
# just keep passing it on infinintly. 

# broadcast msg = 0
# reg msg = 1
# ip request = 2

#ask layer 2 for a function where we get all the interfaces that they communicate on
#ask layer 2 to find a way where we can find which interface the data came from


class StubLayer3:
    def __init__(self, layer_4_cb):
        """Create a Layer 3 Object.

        `layer_4_cb` - Function. This layer will use `layer_4_cb` to pass data to Layer 4. `layer_4_cb`
                       must accept a single parameter `data`.
        """        
        # Save parameter inputs as instance attributes so they can be referred to later
        # python3 ./run-applications.py --input1=1201 --output1=1202 --input2=1203 --output2=1204 --input3=1205 --output3=1206 --superhost=true
        # python3 ./run-applications.py --input1=1202 --output1=1201 --input2=1204 --output2=1203 --input3=1206 --output3=1205 --superhost=false

        cmd_line_key_values = {}
        for full_arg in sys.argv[1:]: # the first argv value is the process name.
            # strip leading hyphens and split on equal signs
            try:
                k, v = full_arg.strip("-").split("=")
            except ValueError as e:
                raise("Your command line argument {full_arg} is wrong. Make sure you follow the format --key=value and you don't use spaces.")
            cmd_line_key_values[k] = v
        #ditionary with the interfaces and what two routers are connected with that interface 
        
        self.layer_4_cb = layer_4_cb

        # Connect to Layer 2
        self.layer2 = Layer2.StubLayer2(self.from_layer_2)

        self.ip = "xx"        
        self.superhost = cmd_line_key_values["superhost"]

        if(self.superhost == "true"):
            self.ip = "10"
            self.iparray = []
            self.iparray.append(self.ip)
            self.seq_num = int(self.ip) * 1000 # create host seqNum; 10,000 (allows 999 mesages to be sent, in theory)

        else: # if not superhost send msg requesting an ip
            self.layer2.from_layer_3("2xx10", interface=1) # temporary
            self.flood_net("2xx10", 3) # not working yet

        self.seq_num_array = []

        # Do you want to send a message without waiting for Layer 4 to call? You
        # can do that any time by calling:
        #   self.layer2.from_layer_3(data, interface=1) 

    def from_layer_4(self, data ):
        """Call this function to send data to this layer from layer 4"""
        logging.debug(f"Layer 3 received msg from Layer 4: {data}")

        # The major job of Layer 3 is to decide which interface to send messages on.
        # You want to "map out" the network (probably using your own Layer 3 messages)
        # and then use some kind of routing algorithm to decide which interface to use.
        # xx = layer 4 dest

        data = self.add_header("1",self.ip, "10",self.send_seq_num(), data) # spotholder
        # need get dest ip function
        ## data = self.add_header("1",self.ip, str(dest_ip),self.send_seq_num(), data)

        # Pass the message down to Layer 2; use interface 1.

        self.layer2.from_layer_3(data, interface=1) # temporary
        self.flood_net(data, 3) # not working yet


    def from_layer_2(self, data):
        """Call this function to send data to this layer from layer 2"""
        logging.debug(f"Layer 3 received msg from Layer 2: {data}")


        # Is this data addressed to this host? If so, pass it up to the next layer.
        # If not, pass it back down to the correct interface on Layer 2.

        # If you're using some kind of "Layer 3 message" to communicate between different
        # hosts' layer 3's (perhaps to do some kind or routing or discoverability), you'll also
        # need to decide whether this data is destined to go to an application (in
        # which case you pass it up to Layer 4) or if it's supposed to be used here in Layer 3.
        # Hopefully you added your own header to it!
        msg = self.parse_message(data)

        if(self.ip == msg[2] and msg[0] == "1"): # check if destip is us and if the msg is a regular msg

            self.layer_4_cb(msg[4]) # send msg to layer4
          
        elif(msg[0] == "1" and msg[2] != self.ip and (not self.check_seq_num(msg[3]))): # if msg is a reg msg and not addressed to us
            ##print("FLOOD THE NETWORK; SEQNUM = " + msg[3])
            self.seq_num_array.append(msg[3]) # record msg seqNum
            self.layer2.from_layer_3(data, interface=1) # keep flooding the net with the msg, (temporary)
            self.flood_net(data, 3) # not working yet


        elif(msg[0] == "0" and self.ip == "xx"): # IP request is filled, assign new ip using the data in the msg
            self.assign_router(data)


        elif(msg[0] == "2" and self.ip == "10"): # respond to the ip broadcast
            self.assign_router(data)

        elif((msg[0] == "0" or msg[0] == "2") and (self.ip != "xx" or self.ip != "10") ): # if my ip has already been assigned keep pushing the msg
            print("FLOODING WITH IP REQUEST/ACK; msg = " + msg)
            self.seq_num_array.append(msg[3]) # record msg seqNum
            self.layer2.from_layer_3(data, interface=1) # keep flooding the net, (temporary)
            self.flood_net(data, 3) # not working yet


        elif( self.check_seq_num(msg[3]) ): # if the message has been seen before do nothing
            ## print("drop the message")
            ## print("Array of seqNums: ", self.seq_num_array)
            pass

          
        # For now let's just forward this up to layer 4.
        #self.layer_4_cb(data)

    def intralayer3_msg(self): # communicate with layer 3

        msgtype = "0"
        msgsrc = "xx"
        msgdest = "10"
        msgseqnum = "00000"

        msg = self.add_header(msgtype, msgsrc, msgdest, msgseqnum, msg)        
        
        # sends msg whenever we want
        self.layer2.from_layer_3(msg, interface=1) # temporary
        self.flood_net(msg, 3) # not working yet


    def parse_message(self, data):
      	#parse the msg data will be the message itself take off one bit from the type, 2 for source, 2 for desti
        #parse information from layer2
        
        msgdata = []
        msgdata.append(data[0])
        msgdata.append(data[1:3])
        msgdata.append(data[3:5])
        msgdata.append(data[5:10])
        msgdata.append(data[10:])
        
        ## ss = "1102510000data"
        ## print("msg type: " + ss[0])
        ## print("ip: " + ss[1:3])
        ## print("dest ip: " + ss[3:5])
        ## print("seqNum: " + ss[5:10])
        ## print("msg: " + ss[10:])

        return msgdata

    def add_header(self, msgtype, msgsrc, msgdest, seqnum, msg):
        #add header information to message from layer 4 send to layer 2
        # msgtype,srcaddy,destaddy
        
        data = msgtype+msgsrc+msgdest+seqnum+msg
        
        return data

    def send_ip(self):
        #layer 4 has requested us a function to send them the instance's IP.
        return self.ip

    def assign_router(self, data):
        msgdata = self.parse_message(data)
        #print("inside assing router, my ip " + self.ip)

        if self.ip == "10": # add ip to table, make an ip for the host that requested

            x = str(len(self.iparray)+10) # client ip's start at 11
            ##print("ip to send IP = " + x)
            self.iparray.append(x)

            self.layer2.from_layer_3("0"+x+"xx"+self.send_seq_num(), interface=1) # send ip back to the host requesting with the new ip masked as the sender ip
            self.flood_net(data, 3) # not working yet

        elif (self.ip =="xx"):
            self.ip = msgdata[1]
            ##print("ip recieved " + self.ip)
            self.seq_num = int(self.ip) * 1000 # create seqNum for node ip*1000, allows for 999 msg's

    def send_seq_num(self): # increment the sequence number everytime this function is called and add it to data that will be sent out
        self.seq_num += 1
        str_seq_num = str(self.seq_num)
        return str_seq_num

    def check_seq_num(self, seq_num):
        x = self.seq_num_array.count(seq_num) # count occurences of said seqNum in seqNumArray
        return x >= 1 # if count is greater than or equal to 1 than the statement is true and there are occurances of the seqnum in the array
            
    def flood_net(self, data, max_interface): # flood the net
        max_interface = max_interface + 1 # add one so the loop interfaces to the x-1 interface

        for x in range(1, max_interface): #send msg over all interfaces
            pass # self.layer2.from_layer_3(data, interface=x)

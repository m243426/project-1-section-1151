import random

# Attributes and Functions

preamble = [1,1,1,1,1,1,1,1]
end_flag = [1,0,1,0,1,0,1,0] + [1,1,0,0,1,1,0,0]

def ascii_to_bin(ascii_str):
    return [int(i) for i in list(''.join(bin(x)[2:].zfill(8) for x in ascii_str.encode('UTF-8')))]
        
def check_sum(int_list):
    nested_int_list = [int_list[i:i+8] for i in range (0,len(int_list),8)]
    out_list = [chunk.count(1)%2 for chunk in nested_int_list]
    out_list2 = [[chunk[j] for chunk in nested_int_list].count(1)%2 for j in range(8)]
    return out_list + out_list2


# practice = [0,0,0,0,0,0,0,0,
#             0,1,0,1,0,1,0,1,
#             0,0,1,1,0,0,1,1,
#             0,0,0,0,1,1,1,1,
#             1,1,1,1,1,1,1,1]
# # print(check_sum(practice))


def package(int_list):
    out = int_list
    out2 = preamble + out + check_sum(out) + end_flag
    return out2
    
def read_header(data):

    if data[:len(preamble)] != preamble:
        print("Error in preamble:", data[:len(preamble)])

    
    if data[-(len(end_flag)):] != end_flag:
        print("Error in endflag:", data[-(len(end_flag)):])

    holder = data[len(preamble):]
    holder = holder[:-(len(end_flag))]                               #need to set to len of checksum and end flag
    check_len = (len(holder)//9) + 8
    check = holder[-(check_len):]
    holder = holder[:-(check_len)]
    if check != check_sum(holder):
        print("Error in checksum:", check, check_sum(holder))
    return holder

def bin_to_ascii(data):
    out = "".join([chr(int("".join([str(j) for j in data[i:i+8]]),2)) for i in range(0,len(data), 8)])
    return out

def randomize(in_list, BER):
    if random.randint(0,BER) == 0:
        error_idx = random.randint(0,len(in_list))
        in_list[error_idx] = (in_list[error_idx] + 1)%1


in_str = input("Enter String:")
while in_str != "":
    out = package(ascii_to_bin(in_str))
    print("Going down:", out)

    randomize(out,2)

    up_data = bin_to_ascii(read_header(out))
    print("Coming up:", up_data)
    
    in_str = input("Enter another string:")
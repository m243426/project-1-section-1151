# This is a stub for Layer 2.

# Import Layers
import sys
sys.path.append('../')
import layer1.mocklayer1 as Layer1
import logging


#put functions here
def check_sum(int_list):
    nested_int_list = [int_list[i:i+8] for i in range (0,len(int_list),8)]
    out_list = [chunk.count(1)%2 for chunk in nested_int_list]
    out_list2 = [[chunk[j] for chunk in nested_int_list].count(1)%2 for j in range(8)]
    return out_list + out_list2


class StubLayer2:
    def __init__(self, layer_3_cb):
        """Create a Layer 2 Object.

        `layer_3_cb` - Function. This layer will use `layer_3_cb` to pass data to Layer 3. `layer_3_cb`
                       must accept a single parameter `data`.
        """
        # Save parameter inputs as instance attributes so they can be referred to later
        self.layer_3_cb = layer_3_cb

        # Connect to Layer 1 on interface 1. Your actual implementation will
        # have multiple interfaces.
        self.layer2_interface_1 = Layer1.MockLayer1(interface_number=1, layer_2_cb=self.from_layer_1,enforce_binary = True)
        self.layer2_interface_2 = Layer1.MockLayer1(interface_number=2, layer_2_cb=self.from_layer_1,enforce_binary = True)
        self.layer2_interface_3 = Layer1.MockLayer1(interface_number=3, layer_2_cb=self.from_layer_1,enforce_binary = True)



        self.preamble = [1,0,1,0,1,0,1,0]*7 + [1,0,1,0,1,0,1,1] 

        self.end_flag = [1,0,1,0,1,0,1,0] + [1,1,0,0,1,1,0,0]

        self.buffer = []
        
        self.waiting_for_more = False

        self.error = False

    def from_layer_3(self, data, interface):
        """Call this function to send data to this layer from layer 3"""
        logging.debug(f"Layer 2 received msg from Layer 3: {data}") #ascii string coming in

        # Layer 2 takes messages from Layer 3, converts the message to bits, and sends that
        # message down to Layer 1. Layer 1 will transmit the exact bitstream that it is sent.
        # So if your Layer 2 implementation will rely on any kind of "start pattern", length
        # field, or "end pattern", you'll need to add it here.

        # Layer 3 is making the decision about which interface to send the data. Layer 2
        # simply follows Layer 3's directions (and receives the interface number as an
        # argument.

        in_data = data

        def ascii_to_bin(ascii_str):
            return [int(i) for i in list(''.join(bin(x)[2:].zfill(8) for x in ascii_str.encode('UTF-8')))]

        def package(int_list):
            out = int_list
            out2 = self.preamble + out + check_sum(out) + self.end_flag
            return out2
    
        def send(interface, bin_data):
            out = package(ascii_to_bin(bin_data))
            if interface == 1:
                self.layer2_interface_1.from_layer_2(out) #return list of ints (1s and 0s)
            elif interface == 2:
                self.layer2_interface_2.from_layer_2(out)
            elif interface == 3:
                self.layer2_interface_3.from_layer_2(out)

        send(interface, in_data)        

    def from_layer_1(self, data):
        """Call this function to send data to this layer from layer 1"""
        logging.debug(f"Layer 2 received msg from Layer 1: {data}") #taking in a list of ints

        # The big goal of Layer 2 is to take incoming bits from Layer 1 and convert them into
        # Layer 3 messages. This is where you will put the logic for that. Think about:
        # 1. How will you know if the bits are actually part of a message, or just noise?
        # 2. How will you know when a message (a Layer 2 frame) begins?
        # 3. How will you know when a message ends?
        # 4. You will almost certainly *not* receive an entire frame at once. You will
        #    need to save incoming data in some kind of buffer (variable).
        # 5. How will you detect errors? What will you do if you detect an error?

        # Let's just forward this up to layer 3.

        def read_header(data):

            if self.waiting_for_more:
                if data[-(len(self.end_flag)):] != self.end_flag:
                    self.buffer += data
                else:
                    data = self.buffer + data
            else:
                if data[:len(self.preamble)] != self.preamble:
                    print("Error in preamble:", data[:len(self.preamble)])
                    self.error = True

                if data[-(len(self.end_flag)):] != self.end_flag:
                    self.buffer = data
                    self.waiting_for_more = True
                    print("Error in endflag:", data[-(len(self.end_flag)):])
                    self.error = True

            holder = data[len(self.preamble):]
            holder = holder[:-(len(self.end_flag))]
            check_len = (len(holder)//9) + 8 #hello
            check = holder[-(check_len):]
            holder = holder[:-(check_len)]
            if check != check_sum(holder):
                print("Error in checksum:", check, check_sum(holder))
                self.error = True
            return holder

        def bin_to_ascii(data):
            out = "".join([chr(int("".join([str(j) for j in data[i:i+8]]),2)) for i in range(0,len(data), 8)])
            return out

        up_data = bin_to_ascii(read_header(data))
        
        if not self.error:
            self.layer_3_cb(up_data) #sending up ascii
        else: 
            self.layer_3_cb("__________Error Detected__________")


